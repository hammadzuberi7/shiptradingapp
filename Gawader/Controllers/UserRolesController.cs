﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Gawader.Areas.Identity.Data;
using Gawader.Models;

namespace Gawader.Controllers
{
    public class UserRolesController : Controller
    {

        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public UserRolesController(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index2()
        {
            return View();
        }

        public async Task<IActionResult> Index()
        {
            var users = await _userManager.Users.ToListAsync();
            var UserRoles = new List<UserRolesViewModel>();
            //var roles = await _roleManager.Roles.ToListAsync();

            foreach (AppUser user in users)
            {
                var thisUserModel = new UserRolesViewModel();
                thisUserModel.UserId = user.Id;
                thisUserModel.UserName = user.UserName;
                thisUserModel.Email = user.Email;
                thisUserModel.FirstName = user.FirstName;
                thisUserModel.LastName = user.LastName;
                thisUserModel.Roles = await GetUserRoles(user);

                UserRoles.Add(thisUserModel);

            }
            return View(UserRoles);

        }

        private async Task<List<string>> GetUserRoles(AppUser user)
        {

            return new List<string>(await _userManager.GetRolesAsync(user));
        }


        [Authorize(Roles = "SuperAdmin")]
        public async Task<IActionResult> Manage(string UserId)
        {
            ViewBag.userId = UserId;

            var user = await _userManager.FindByIdAsync(UserId);

            if (UserId == null || user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {UserId} cannot be found";
                return View("NotFound");
            }

            ViewBag.UserName = user.UserName;

            var model = new List<ManageUserRolesViewModel>();

            foreach (var role in _roleManager.Roles)
            {
                var userRolesviewModel = new ManageUserRolesViewModel()
                {
                    RoleId = role.Id,
                    RoleName = role.Name,
                };

                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    userRolesviewModel.Selected = true;
                }
                else
                {
                    userRolesviewModel.Selected = false;
                }
                model.Add(userRolesviewModel);

            }
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "SuperAdmin")]
        public async Task<IActionResult> Manage(List<ManageUserRolesViewModel> model, string UserId)
        {
            var user = await _userManager.FindByIdAsync(UserId);

            if (UserId == null || user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {UserId} cannot be found";
                return View();
            }
            var roles = await _userManager.GetRolesAsync(user);
            var result = await _userManager.RemoveFromRolesAsync(user, roles);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Can not remove user existing role.");
                return View(model);

            }


            result = await _userManager.AddToRolesAsync(user, model.Where(x => x.Selected).Select(y => y.RoleName));
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Can not add selected role to the user.");
                return View(model);

            }

            return RedirectToAction("Index");

        }
    }
}
