﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Gawader.Data;
using Gawader.Models;
using System.Security.Claims;
namespace Gawader.Controllers
{
    public class ShipsController : Controller
    {

        private readonly GawaderContext _dbContext;
        public ShipsController(GawaderContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public async Task<IActionResult> Index()
        {
            var ships = await _dbContext.ShipModels.ToListAsync();

            var vwShips = new List<ShipsViewModel>();

            foreach (Ships sh in ships)
            {
                var thisShipModel = new ShipsViewModel();
                thisShipModel.ShipIndex = sh.ShipIndex;
                thisShipModel.ShipName = sh.ShipName;
                thisShipModel.ShipDescription = sh.ShipDescription;
                thisShipModel.AnchorNumber = sh.AnchorNumber;
                thisShipModel.CategoryId = sh.CategoryId;
                thisShipModel.CountryIndex = sh.CountryIndex;
                thisShipModel.NumberOfItems = sh.NumberOfItems;

                vwShips.Add(thisShipModel);
            }

            return View(vwShips);

        }

        [HttpGet]
        public IActionResult PostShips()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> PostShips(ShipsViewModel vwship)
        {
            if (ModelState.IsValid)
            {
                Ships ships = new Ships();

                ships.ShipName = vwship.ShipName;
                ships.ShipDescription = vwship.ShipDescription;

                ships.ShipCapacity = vwship.ShipCapacity;
                ships.AnchorNumber = vwship.AnchorNumber;
                ships.CountryIndex = vwship.CountryIndex;

                ships.CategoryId = vwship.CategoryId;
                ships.NumberOfItems = vwship.NumberOfItems;
                ships.DateOfArrival = vwship.DateOfArrival;
                ships.EnterAt = DateTime.Now;

                var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                ships.EnterBy = userId;


                await _dbContext.ShipModels.AddAsync(ships);
                await _dbContext.SaveChangesAsync();

            }

            return RedirectToAction("Index");
        }
    }
}
