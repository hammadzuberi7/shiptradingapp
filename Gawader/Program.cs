using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Gawader.Areas.Identity.Data;
using Gawader.Data;
var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("TraderAppContextConnection") ?? throw new InvalidOperationException("Connection string 'TraderAppContextConnection' not found.");

builder.Services.AddDbContext<GawaderContext>(options =>
    options.UseSqlServer(connectionString));;

builder.Services.AddDefaultIdentity<AppUser>(options => options.SignIn.RequireConfirmedAccount = false)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<GawaderContext>();;

// Add services to the container.
builder.Services.AddControllersWithViews();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}


try
{

    using (var scope = app.Services.CreateScope())
    {
        var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
        var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

        await ContextSeed.SeedRolesAsync(userManager, roleManager);
        await ContextSeed.SeedSuperAdminAsync(userManager, roleManager);

    }
}
catch (Exception ex)
{
    //var logger = loggerFactory.CreateLogger<Program>();
    //logger.LogError(ex, "An error occurred seeding the DB.");
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();;

app.UseAuthorization();

app.MapAreaControllerRoute(

    name: "default",
    areaName: "Identity",
   pattern: "{controller=Account}/{action=Login}/{id?}");


app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}"
    );
app.MapRazorPages();

app.Run();
