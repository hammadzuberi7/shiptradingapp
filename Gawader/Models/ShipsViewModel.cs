﻿using System.ComponentModel.DataAnnotations;

namespace Gawader.Models
{
    public class ShipsViewModel
    {
        [Key]
        public int ShipIndex { get; set; }

        [StringLength(100), Required]
        public string ShipName { get; set; } = "";

        [StringLength(500)]
        public string ShipDescription { get; set; } = "";

        public int ShipCapacity { get; set; }

        public Countries? CountryIndex { get; set; }
        public DateTime DateOfArrival { get; set; }

        [StringLength(50)]
        public string? AnchorNumber { get; set; }

        public string CategoryId { get; set; }
        public int NumberOfItems { get; set; }

    }
}
