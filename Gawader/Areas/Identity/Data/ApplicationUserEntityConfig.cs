﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Gawader.Areas.Identity.Data;

namespace Gawader.Data
{
    internal class ApplicationUserEntityConfig : IEntityTypeConfiguration<AppUser>
    {

        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
            builder.Property(p => p.FirstName).HasMaxLength(255);
        }
    }
}