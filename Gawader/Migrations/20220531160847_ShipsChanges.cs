﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Gawader.Migrations
{
    public partial class ShipsChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UpdateBy",
                schema: "Identity",
                table: "Ship",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "EnterBy",
                schema: "Identity",
                table: "Ship",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryId",
                schema: "Identity",
                table: "Ship",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                schema: "Identity",
                table: "AspNetUsers",
                type: "nvarchar(256)",
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(256)",
                oldMaxLength: 256);

            migrationBuilder.CreateTable(
                name: "ShipsViewModel",
                schema: "Identity",
                columns: table => new
                {
                    ShipIndex = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShipName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ShipDescription = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    ShipCapacity = table.Column<int>(type: "int", nullable: false),
                    CountryIndexID = table.Column<int>(type: "int", nullable: true),
                    DateOfArrival = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AnchorNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CategoryId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NumberOfItems = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipsViewModel", x => x.ShipIndex);
                    table.ForeignKey(
                        name: "FK_ShipsViewModel_Country_CountryIndexID",
                        column: x => x.CountryIndexID,
                        principalSchema: "Identity",
                        principalTable: "Country",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_ShipsViewModel_CountryIndexID",
                schema: "Identity",
                table: "ShipsViewModel",
                column: "CountryIndexID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ShipsViewModel",
                schema: "Identity");

            migrationBuilder.AlterColumn<int>(
                name: "UpdateBy",
                schema: "Identity",
                table: "Ship",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<int>(
                name: "EnterBy",
                schema: "Identity",
                table: "Ship",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<int>(
                name: "CategoryId",
                schema: "Identity",
                table: "Ship",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                schema: "Identity",
                table: "AspNetUsers",
                type: "nvarchar(256)",
                maxLength: 256,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(256)",
                oldMaxLength: 256,
                oldNullable: true);
        }
    }
}
