﻿using System.ComponentModel.DataAnnotations;

namespace Gawader.Models
{
    public class Ships
    {
        [Key]
        public int ShipIndex { get; set; }

        [StringLength(100), Required]
        public string ShipName { get; set; } = "";

        [StringLength(500)]
        public string? ShipDescription { get; set; } = "";

        public int ShipCapacity { get; set; }

        public Countries? CountryIndex { get; set; }
        public DateTime? DateOfArrival { get; set; }

        [StringLength(50)]
        public string? AnchorNumber { get; set; }


        public string ?EnterBy { get; set; }
        public DateTime EnterAt { get; set; }

        public string? UpdateBy { get; set; }
        public DateTime? UpdateAt { get; set; }
        public string CategoryId { get; set; }
        public int NumberOfItems { get; set; }

    }



    public class Countries
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string CountryName { get; set; } = "";
    }
}
