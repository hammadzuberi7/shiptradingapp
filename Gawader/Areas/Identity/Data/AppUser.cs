﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Gawader.Areas.Identity.Data;

// Add profile data for application users by adding properties to the AppUser class
public class AppUser : IdentityUser
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateTime DateOfBirth { get; set; }

    [Display(Name = "CNIC"),
        RegularExpression("^[0-9]{5}-[0-9]{7}-[0-9]$",
        ErrorMessage = "CNIC No must follow the XXXXX-XXXXXXX-X format!")]
    public string CNIC { get; set; }
    public string ContactNumber { get; set; }
    
}

public enum Roles
{
    SuperAdmin,
    Admin,
    Moderator,
    Basic
}